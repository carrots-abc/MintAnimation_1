using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/GraphicColor")]
    public class MintAnimGraphicColor : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerGraphicColor MintPlayer;
        public override Core.MintAnim.MintPlayerBase  MintAnimation() { return this.MintPlayer; }
    }
}