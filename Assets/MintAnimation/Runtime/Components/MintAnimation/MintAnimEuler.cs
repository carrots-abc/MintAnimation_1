using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/Euler")]
    public class MintAnimEuler : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerEuler MintPlayer;
        public override Core.MintAnim.MintPlayerBase     MintAnimation() { return this.MintPlayer; }
    }
}