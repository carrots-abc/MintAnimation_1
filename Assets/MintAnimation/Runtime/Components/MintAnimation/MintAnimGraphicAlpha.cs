using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/GraphicAlpha")]
    [RequireComponent(typeof(CanvasGroup))]
    public class MintAnimGraphicAlpha : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerGraphicAlpha MintPlayer;
        public override Core.MintAnim.MintPlayerBase  MintAnimation() { return this.MintPlayer; }
    }
}