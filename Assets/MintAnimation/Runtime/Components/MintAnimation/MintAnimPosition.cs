using MintAnimation.Runtime.Core.MintAnim;
using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/Position")]
    public class MintAnimPosition : MintAnimBase
    {
        public Core.MintAnim.MintPlayerPosition MintPlayer;
        public override Core.MintAnim.MintPlayerBase MintAnimation() { return this.MintPlayer; }
    }
}