namespace MintAnimation.Runtime.Components
{
    public interface IMintAnim
    {
        void StartAnim();
        void PauseAnim();
        void ResumeAnim();
        void StopAnim();
    }
}