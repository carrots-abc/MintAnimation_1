using System;
using MintAnimation.Core;
using UnityEngine;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerScale : MintPlayerBase
    {

        public Vector3 StartValue = Vector3.zero;
        public Vector3 EndValue   = Vector3.one;

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = playerMix.StartV3;
            this.EndValue   = playerMix.EndV3;
        }

        protected override IMintChannel Channel() { return new MintChannelVector3(this.OnUpdate, this.StartValue, this.EndValue); }

        public override void OnAutoStartValue() { this.StartValue = this._transform.localScale; }

        private void OnUpdate(Vector3 obj) { this._transform.localScale = obj; }

    }
}