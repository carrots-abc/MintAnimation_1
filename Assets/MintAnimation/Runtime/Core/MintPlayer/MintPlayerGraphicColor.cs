using MintAnimation.Core;
using UnityEngine;
using UnityEngine.UI;

namespace MintAnimation.Runtime.Core.MintAnim
{
    public class MintPlayerGraphicColor : MintPlayerBase
    {
        public Color StartValue = Color.white;
        public Color EndValue = Color.white;

        private Graphic _graphic;

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = playerMix.StartCol;
            this.EndValue = playerMix.EndCol;
        }

        protected override IMintChannel Channel() { return new MintChannelColor(this.OnUpdate, this.StartValue, this.EndValue); }

        public override bool Init(Transform transform)
        {
            this._graphic = transform?.GetComponent<Graphic>();
            if (this._graphic == null)
            {
                Debug.LogError("缺少关键组件： Graphic");
                return false;
            }
            return base.Init(transform);
        }
        private void OnUpdate(Color obj) { this._graphic.color = obj; }

        public override void OnAutoStartValue() { this.StartValue = this._graphic.color; }

    }
}