using UnityEngine;

namespace MintAnimation.Tests.ReorderableListTest
{
    [System.Serializable]
    public class ListItem
    {

        public string Name;
        public string Table;
        public bool   IsColor;
        public Color  Color;

    }
}