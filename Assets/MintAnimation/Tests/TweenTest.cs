/*
* Author: Foldcc
* Mail:   lhyuau@qq.com
*/

using System;
using MintAnimation.Core;

namespace UnityEngine
{
    public class TweenTest : UnityEngine.MonoBehaviour
    {

        public bool IsRandom = true;

        public Vector3 startValue;
        public Vector3 endValue;
        
        public MintTweenOptions Options;

        public MintTween Tween;

        private void Awake()
        {
            if (this.IsRandom)
            {
                this.Options                                        = new MintTweenOptions();
                this.transform.GetComponent<SpriteRenderer>().color = new Color(Random.value, Random.value, Random.value);
                var x = Random.Range(-8, 8f);
                var y = Random.Range(-1, -5f);
                this.startValue       = new Vector3(x , y);
                this.endValue         = new Vector3(x , -y);
                this.Options.Duration = Random.Range(1, 5f);
                this.Options.EaseType = (MintEaseMethod) Random.Range(0, 31);
                this.Options.IsBack   = true;
                this.Options.IsLoop   = true;
            }
            this.Tween = new MintTween(this.Updater , this.startValue , this.endValue , this.Options);
        }

        private void OnEnable()
        {
            this.Tween.Start();
        }

        private void Updater(Vector3 process)
        {
            this.transform.position = process;
        }
        private void OnDisable()
        {
            this.Tween.End();
        }
    }
}