# MintAnimation

![](https://img.shields.io/badge/Unity-5.0-blue) ![](https://img.shields.io/badge/Unity-2018-green)  ![](https://img.shields.io/badge/Unity-2019-red)

## 介绍
MintAnimation是一款轻量级动画插件，可用于UI的缓动动画以及其他游戏对象的补间动画。

此外MintAnimation不依赖任何第三方插值动画库，自己本身具备一套插值运算驱动，所有的动画组件都是基于此开发，并且做到了0 GC Alloc，效率也极为出众。

该插值库(MintAnimation.Core)支持扩展，用法与DoTween类似,使用者可以轻松的编写更加复杂的自定义动画。

**新版本正在开发中,不建议使用，旧版本更加稳定：** [MintAnimation_V1](https://gitee.com/Foldcc/MintAnimation/tree/MintAnimation_V1/)